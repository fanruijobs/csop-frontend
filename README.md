This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Necessary Parts
### `navigation`
Route navigation is used here (react-route).
This part defines the routes available in the project. Public and **PRIVATE** route can be defined here. 

### `pages`
Pages means different web-pages. Pages are class-based component. Only pages have states.

### `components`
Components means different reusable, stateless and function-based components.

### `redux`
Redux code part. Actions, reducers defined here. Api-calling functions imported from apis folder.

### `apis`
Here api-calling functions are defined.

### `utils`
Small helper functions.

### `constants`
To fake api-calling results.

## Run the project locally
~~~sh
PORT=xxxx npm start
~~~

## Other Necessary files.
### nginx.conf
Nginx settings inside the docker.

### Dockerfile
No need to change

### docker-compose.yml
This is config of docker-compose. This gives extendability.

### project-name-react-subdomain.conf
This is config of nginx outside the docker. The subdomain name is defined here.

### .cicleci/config.yml
This is config file of circleci. Information about the hosting machine is defined here.

## Thing to change before developing/deploying new project
Need to go through carefully before deploying.

### CircleCi
1. Set Up CircleCi server.
2. Cmd-C/V VPC host's private-key to CircleCi.
3. Project Name in CircleCi config file.
4. ...

### docker-compose.yml
Change the PORT number and project name.

### project-name-react-subdomain.conf
Change the following:
1. File Name
2. PORT number
3. subdomain name






## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `yarn build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
