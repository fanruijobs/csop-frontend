import {serverUrl} from '../config';

// const byDefault = (token, ) => {
const list = () => {

    return fetch(serverUrl + '/ftp/status/', {
        method: "GET"
    }).then(response => {
        if (response.ok) {
            return response.json();
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
        },
        error => {
            var errmess = new Error(error.message);
            throw errmess;
        }
    )
}


export default {list, }