const env = process.env.NODE_ENV; // 'development' or 'production' or ...
console.log("env: ", env);

let serverUrl, appUrl

if (env==="production"){
    serverUrl = "https://p1tcdmk1r7.execute-api.ap-southeast-1.amazonaws.com/dev"; // App Backend Server
    appUrl = "http://csop.ricky89.com"; // App Frontend URL
    
} else {
    serverUrl = "http://localhost:9400"; // App Backend Server
    appUrl = "http://localhost:4800"; // App Frontend URL
}

export { serverUrl, appUrl };