import React from 'react';
import { Container } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLinkedin } from '@fortawesome/free-brands-svg-icons'

const Footer = (props) => {
    console.log("Loading footer.")
    return (
        <footer>
            <Container>                
                <div className="row justify-content-center">             
                    <div className="col-auto">
                        <br />
                        <br />
                        <br />
                        <br />
                        <a href="https://www.linkedin.com/in/fanruisingapore/"><FontAwesomeIcon icon={faLinkedin} size="2x" /></a>
                    </div>
                </div>
            </Container>
        </footer>
    );
}

export default Footer