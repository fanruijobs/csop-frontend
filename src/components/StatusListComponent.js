import React from 'react';
import { ListGroup, ListGroupItem, Badge } from 'reactstrap';
import moment from 'moment'

const StatusListGroupItem = (props) => {
    return (
        <ListGroupItem> 
            <h6> 
                {/* {props.status.attemptTime}  */}
                {moment(props.status.attemptTime).format('LLLL')}
                {props.status.isSuccessful === true 
                    ? <Badge pill color="success" style={{ float: "right" }}> Success </Badge> 
                    : <Badge pill color="danger" style={{ float: "right" }}> Fail </Badge>}
            </h6>
        </ListGroupItem>
    )
}

const StatusList = (props) => {
    
    if (props.statuses.length === 0){
        return (
            <h2> No Transactions. </h2>
        )
    }

    return (
        <ListGroup>
            {props.statuses.map(status => 
                {   
                    if (status.isSuccessful===true&&props.showSuccess===false){
                        return null
                    } else if (status.isSuccessful===false&&props.showFailure===false){
                        return null
                    } else {
                        return (<StatusListGroupItem status={status}/> )
                    }
                }
            )}
        </ListGroup>
    );
}

export default StatusList;

