import React from 'react';

import { Nav, NavItem, Navbar, NavbarBrand,
     Collapse, NavbarToggler, NavLink } from 'reactstrap';

import { Link } from 'react-router-dom';

const Header = (props) => {

    return (  
        <Navbar color="light" light expand="md" >
            <Nav navbar>
                <NavbarBrand className="mr-auto" href="/">
                    <Link to="/">
                        <img src="/favicon.ico" height="30" width="30" alt="Ricky" />
                    </Link>
                    <span> FTP Status </span>
                </NavbarBrand>
            </Nav>
            <NavbarToggler onClick={props.toggle}/>
            <Collapse isOpen={props.isDropDownOpen} navbar>
                <Nav className="ml-auto" navbar>
                    <NavItem>
                        <NavLink href="/"> Home </NavLink>
                    </NavItem>
                </Nav>
            </Collapse>
        </Navbar>
    )
}


export default Header;