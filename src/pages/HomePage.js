import React, { Component } from 'react';
import { Container, Button } from 'reactstrap';

import Header from '../components/HeaderComponent';
import Footer from '../components/FooterComponent';
import TopPart from '../components/TopPartComponent'
import StatusList from '../components/StatusListComponent'

import apis from '../apis'
class Home extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isHeaderDropDownOpen: false,
            showSuccess: true,
            showFailure: true,
            isStatusLoaded: false,
            statuses:{

            },
        };

        this.toggleHeaderDropdown = this.toggleHeaderDropdown.bind(this);
        this.onClickShowSuccess = this.onClickShowSuccess.bind(this);
        this.onClickShowFailure = this.onClickShowFailure.bind(this)
    }

    onClickShowSuccess() {
        this.setState({
            showSuccess: !this.state.showSuccess
        });
    }

    onClickShowFailure() {
        this.setState({
            showFailure: !this.state.showFailure
        });
    }

    toggleHeaderDropdown() {
        this.setState({
            isHeaderDropDownOpen: !this.state.isHeaderDropDownOpen
        });
    }

    componentDidMount(){
        
        if (this.state.isStatusLoaded === false){
            apis.statuses.list()
            .then(statuses => {
                console.log('statuses: ', statuses)
                this.setState({ "statuses": statuses, "isStatusLoaded": true})
            })
            .catch(error => {
                console.log(error);
            });
        }
    }

    render(){
        var information = {
            "title": "A Simple Dashboard",
            "descriiption": "A Simple Dashboard to show ftp status.",
            "paragraph": "There is no long description. It is just a simple app. The status of each ftp attempt will be shown here."
        }
        return(
            <Container style={styles.container}>
                <Header 
                    isDropDownOpen={this.state.isHeaderDropDownOpen}
                    toggle={this.toggleHeaderDropdown}
                    />
                <TopPart title={information.title} 
                        description={information.descriiption}
                        paragraph={information.paragraph} 
                        />
                <Button color="success" onClick={this.onClickShowSuccess}>Show Success</Button>
                &nbsp;&nbsp;&nbsp;&nbsp; 
                <Button color="danger" onClick={this.onClickShowFailure}>Show Failure</Button>
                <br />
                <br />
                {this.state.isStatusLoaded===true ? <StatusList statuses={this.state.statuses} showSuccess={this.state.showSuccess} showFailure={this.state.showFailure}/> : <h1> Loading ...</h1>}
                <Footer />
            </Container>
        );
    }
}

const styles = {
    "container": {
        "padding-right": 0,
        "padding-left": 0,
        "margin-right": "auto",
        "margin-left": "auto"
    }
}

export default Home;   