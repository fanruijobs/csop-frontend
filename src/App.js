import React from 'react';
import { BrowserRouter } from 'react-router-dom';

import Navigation from './navigation/Navigation';

const App = (props) => {
  return (
    <BrowserRouter>
      <Navigation/>
    </BrowserRouter>
  );
}

export default App;

