import React, { Component } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom'

// Basic Pages
import Home from "../pages/HomePage";

class Navigation extends Component {
    render() {
        return (
            <Switch>
                <Route exact path="/"  component={(props) => <Home {...props} auth={this.props.auth} />} />
            </Switch> 
        )
    }
}
export default withRouter(Navigation);
